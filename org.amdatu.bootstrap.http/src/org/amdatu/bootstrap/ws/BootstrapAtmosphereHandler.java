/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.ws;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.amdatu.bootstrap.exceptions.CommandNotFoundException;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResponse;
import org.atmosphere.handler.OnMessage;
import org.atmosphere.websocket.WebSocketEventListenerAdapter;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

import rx.Observable;
import rx.Subscriber;
import aQute.bnd.annotation.metatype.Configurable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component(
		provides = { OnMessage.class, EventHandler.class },
		properties = {@Property(name=EventConstants.EVENT_TOPIC, values=Navigator.CHANGEDIR_TOPIC)}
		)
public class BootstrapAtmosphereHandler extends OnMessage<String> implements EventHandler {
	

	@ServiceDependency
	private volatile PluginRegistry m_pluginRegistry;

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@Inject
	private volatile DependencyManager m_dm;
	
	private volatile AtmosphereResource m_resource;
	
	private volatile ExecutorService m_commandExecutor = Executors.newSingleThreadExecutor();
	
	private volatile Subscriber<Object> m_subscriber;
	
	@Override
	public void onOpen(AtmosphereResource resource) throws IOException {
		
		if(m_resource != null) {
			// this means bootstrap is open in several browser windows
			String errorMessage = "Amdatu Bootstrap is already running in another browser window. ";
			errorMessage += "Please close this window.";
			Exception exception = new Exception(errorMessage);
			resource.write(new BootstrapMessageFactory().createAsBytes("error", exception));
			return;
		}
		
		super.onOpen(resource);
		
		m_resource = resource;
		Observable<Object> observable = Observable.create(s-> m_subscriber = s);		
		Prompt atmospherePrompt = new AtmospherePrompt(resource, observable.toBlocking());
		
		org.apache.felix.dm.Component component = m_dm.createComponent().setInterface(AtmosphereResource.class.getName(), null).setImplementation(m_resource);
		m_dm.add(component);
		
		resource.addEventListener(new WebSocketEventListenerAdapter() {
			@Override
			public void onMessage(@SuppressWarnings("rawtypes") WebSocketEvent  event) {
				ObjectMapper mapper = new ObjectMapper();
				
				WsMessage wsMessage;
				try {
					wsMessage = mapper.readValue(event.message().toString(), WsMessage.class);
					switch (wsMessage.getType()) {
					case "promptquestion":
						handlePromptQuestion(event, mapper);
						break;
					case "promptmultiquestion":
						handlePromptMultiQuestion(event, mapper);
						break;
					case "cancel":
						m_subscriber.onError(new InterruptedException());
						break;
					case "command":
						m_commandExecutor.execute(() -> {
							try {
								handleCommand(event, mapper, atmospherePrompt);
							} catch(CommandNotFoundException e) {
								resource.write(new BootstrapMessageFactory().createAsBytes("error", e));
							}
							catch (Exception e) {
								resource.write(new BootstrapMessageFactory().createAsBytes("error", e.getCause()));
								e.printStackTrace();
							}
						});
						break;
					default: System.out.println("Unrecognized message: " + wsMessage);
					}
				} catch (Exception e) {
					resource.write(new BootstrapMessageFactory().createAsBytes("error", e));
				}

			}

			private void handleCommand(@SuppressWarnings("rawtypes") WebSocketEvent event, ObjectMapper mapper, Prompt atmospherePrompt) throws Exception {
				CommandInput input = mapper.readValue(event.message().toString(), CommandInput.class);
				
				String[] name = input.getName().split("-");
												
				Optional<BootstrapPlugin> plugin = m_pluginRegistry.getPlugin(name[0]);
				if(!plugin.isPresent()) {
					throw new CommandNotFoundException(input.getName());
				}
				
				Method method = Arrays.stream(plugin.get().getClass().getMethods())									
									.filter(m->m.getName().equals(name[1]))
									.findAny()
									.orElseThrow(() -> new CommandNotFoundException(input.getName()));;
				
				Class<?>[] parameterTypes = method.getParameterTypes();
				Object result;
				
				if (parameterTypes.length > 0) {
					List<?> configurables = Arrays.stream(parameterTypes)
							.map(p -> {
								if(p.isAssignableFrom(Prompt.class)) {
									return atmospherePrompt;
								} else {
									return Configurable.createConfigurable(p, input.getArgs());
								}
								
							}).collect(Collectors.toList());
					
					
					Object[] params = new Object[configurables.size()];
					
					result = method.invoke(plugin.get(), configurables.toArray(params));
					
				} else {
					result = method.invoke(plugin.get());
				}
				
				
				CommandResult commandResult = new CommandResult(input.getName(), result);
				resource.write(new BootstrapMessageFactory().createAsBytes("command-result", commandResult));
			}

			private void handlePromptQuestion(@SuppressWarnings("rawtypes") WebSocketEvent event, ObjectMapper mapper) throws IOException,
					JsonParseException, JsonMappingException {
				PromptQuestion q = mapper.readValue(event.message().toString(), PromptQuestion.class);
				m_subscriber.onNext(q.getAnswer());
			}
			
			private void handlePromptMultiQuestion(@SuppressWarnings("rawtypes") WebSocketEvent event, ObjectMapper mapper) throws JsonParseException, JsonMappingException, IOException {
				@SuppressWarnings("rawtypes")
				PromptMultiQuestion q = mapper.readValue(event.message().toString(), PromptMultiQuestion.class);
				m_subscriber.onNext(q.getAnswers());
			}
			
		});
	}
	
	@Override
	public void onMessage(AtmosphereResponse arg0, String arg1)
			throws IOException {
	}
	
	@Override
	public void onDisconnect(AtmosphereResponse response) throws IOException {
		super.onDisconnect(response);
		if(m_resource != null && m_resource.equals(response.resource())) {
			m_resource = null;
		}
	}

	@Override
	public void handleEvent(Event event) {
		if(event.getTopic().equals(Navigator.CHANGEDIR_TOPIC)) {
			Path dir = (Path)event.getProperty("dir");
			CommandResult commandResult = new CommandResult("navigation-cd", dir.toFile());
			m_resource.write(new BootstrapMessageFactory().createAsBytes("command-result", commandResult));
			
			ScopeUpdate scopeUpdate = new ScopeUpdate(m_navigator.getCurrentScope());
			m_resource.write(new BootstrapMessageFactory().createAsBytes("scope-update", scopeUpdate));
		}
	}
	
	class ScopeUpdate {
		private final String m_type = "scopeUpdate";
		private final String m_newScope;
		
		public ScopeUpdate(Scope newScope) {
			m_newScope = newScope.toString();
		}
		public String getType() {
			return m_type;
		}
		public String getNewScope() {
			return m_newScope;
		}
	}
}
