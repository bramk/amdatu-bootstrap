/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.webui;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.DynamicSelect;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.plugins.amdatu.services.WebResourcesService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.ProjectConfigurator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.template.Template;
import org.amdatu.bootstrap.template.TemplateException;
import org.amdatu.bootstrap.template.TemplateProcessor;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class WebUIPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile WebResourcesService m_webResourcesService;
	
	private final Set<TemplateProvider> m_projectTemplateProviders = new HashSet<>();
	
	@ServiceDependency
	private volatile TemplateProcessor m_templateProcessor;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile ProjectConfigurator m_projectConfigurator;
	
	interface WebInstallArgs extends Parameters {
		@Required
		@Description("Path to register")
		String path();
		
		@Description("Path to the bnd file")
		@Required
		@ProjectBndFile
		File bndfile();
		
		@DynamicSelect("listTemplates")
		String template();
	}
	
	public List<EnumValue> listTemplates() {
		return m_projectTemplateProviders.stream()
				.flatMap(p -> p.listTemplates().stream())
				.map(t -> new EnumValue(t.getName(), t.getName()))
				.collect(Collectors.toList());
	}
	
	@Command
	@Description("Setup a web project based on a template")
	public InstallResult install(WebInstallArgs args, Prompt prompt) {
		if(m_navigator.getCurrentDir().resolve("dist").toFile().exists()) {
			throw new RuntimeException("Directory 'dist' already exists in project");
		}
		
		if(m_navigator.getCurrentDir().resolve("app").toFile().exists()) {
			throw new RuntimeException("Directory 'app' already exists in project");
		}
			
			
		Path bndFile = m_navigator.getCurrentDir().resolve(args.bndfile().toPath());
		m_webResourcesService.addWebResourceHeaders(args.path(), "index.html", "dist/app", bndFile);
		
		prompt.println("Added Web Resource headers");
		
		Optional<Template> template = m_projectTemplateProviders.stream()
			.flatMap(p -> p.listTemplates().stream())
			.filter(t -> t.getName().equals(args.template()))
			.findAny();
		
		if(template.isPresent()) {
			Map<String, Object> properties = new HashMap<>();
			
			try {
				m_templateProcessor.installTemplate(template.get(), m_navigator.getCurrentDir().toFile(), properties);
			} catch (TemplateException e) {
				throw new RuntimeException(e);
			}
		}
		
		m_dependencyBuilder.addDependencies(Dependency.fromStrings("osgi.cmpn", "org.apache.felix.http.servlet-api", "org.amdatu.template.processor"));
		
		
		m_projectConfigurator.addPrivatePackage(bndFile, "org.amdatu.bootstrap.webui.impl");
		m_projectConfigurator.addExportedPackage(bndFile, "org.amdatu.bootstrap.webui");
		m_projectConfigurator.addEntryToHeader(bndFile, "Include-Resource", "Include-Resource: dist/app=dist/app,resources=processed-resources");
		
		return InstallResult.empty();
	}
	
	@Override
	public String getName() {
		return "webui";
	}

	@ServiceDependency(removed="templateProviderRemoved", filter="(type=webuiproject)")
	private void templateProviderAdded(TemplateProvider templateProvider){
		m_projectTemplateProviders.add(templateProvider);
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void templateProviderRemoved(TemplateProvider templateProvider) {
		m_projectTemplateProviders.remove(templateProvider);
	}
}
