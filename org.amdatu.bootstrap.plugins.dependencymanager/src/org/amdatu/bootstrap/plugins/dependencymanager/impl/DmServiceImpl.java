/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencymanager.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.FullyQualifiedName;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.plugins.dependencymanager.DmVersion;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.ResourceManager;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

@Component
public class DmServiceImpl implements DmService {
	private static final String ANNOTATION_PLUGIN_NAME = "org.apache.felix.dm.annotation.plugin.bnd.AnnotationPlugin";

	public static final String ANNOTATION_PROCESSOR_JAR_DM3 = "org.apache.felix.dependencymanager.annotation-3.2.0.jar";
	
	public static final String ANNOTATION_PROCESSOR_JAR_DM4 = "org.apache.felix.dependencymanager.annotation.jar";

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;

	@ServiceDependency(required = true)
	private volatile TemplateEngine m_templateEngine;

	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Override
	public InstallResult installAnnotationProcessor(DmVersion version, Path bndFile) {
		Path mainBnd = m_navigator.getWorkspaceDir().resolve("cnf/build.bnd");

		try {
			String mainBndContents = new String(Files.readAllBytes(mainBnd));

			if((version.equals(DmVersion.DM3) && !mainBndContents.contains(ANNOTATION_PROCESSOR_JAR_DM3)) 
					|| (version.equals(DmVersion.DM4) && !mainBndContents.contains(ANNOTATION_PROCESSOR_JAR_DM4))) {
				
				List<String> filteredLines = Files.readAllLines(mainBnd).stream().filter(line -> !line.contains(ANNOTATION_PLUGIN_NAME)).collect(Collectors.toList());
				Files.write(mainBnd, filteredLines, StandardOpenOption.TRUNCATE_EXISTING);
				
				BndEditModel model = new BndEditModel();
				model.loadFrom(mainBnd.toFile());

				List<HeaderClause> plugins = model.getPlugins();

				if (plugins == null) {
					plugins = new ArrayList<>();
				}

				if (plugins.isEmpty()) {
					plugins.add(createRepoPluginHeader());
				}

				plugins.add(createAnnotationPluginHeader(version));

				model.setPlugins(plugins);

				Document document = new Document(new String(Files.readAllBytes(mainBnd)));
				model.saveChangesTo(document);

				m_resourceManager.writeFile(mainBnd, document.get().getBytes());

				Path pluginsDir = m_navigator.getWorkspaceDir().resolve("cnf/plugins");
				
				if(version.equals(DmVersion.DM3)) {
					try (InputStream in = m_bundleContext.getBundle().getEntry("/libs/" + ANNOTATION_PROCESSOR_JAR_DM3).openStream()) {
						Files.copy(in, pluginsDir.resolve(ANNOTATION_PROCESSOR_JAR_DM3), StandardCopyOption.REPLACE_EXISTING);
					}
				
					return m_dependencyBuilder.addDependency(new Dependency("../cnf/plugins/org.apache.felix.dependencymanager.annotation-3.2.0.jar", "file"), bndFile);
				} else {
					try (InputStream in = m_bundleContext.getBundle().getEntry("/libs/" + ANNOTATION_PROCESSOR_JAR_DM4).openStream()) {
						Files.copy(in, pluginsDir.resolve(ANNOTATION_PROCESSOR_JAR_DM4), StandardCopyOption.REPLACE_EXISTING);
					}
					
					return m_dependencyBuilder.addDependency(new Dependency("../cnf/plugins/org.apache.felix.dependencymanager.annotation.jar", "file"), bndFile);
				}
			} else {
				return m_dependencyBuilder.addDependency(new Dependency("../cnf/plugins/org.apache.felix.dependencymanager.annotation.jar", "file"), bndFile);
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public InstallResult installAnnotationProcessor(DmVersion version) {
		return installAnnotationProcessor(version, m_navigator.getBndFile());
	}
	
	@Override
	public InstallResult installAnnotationProcessor() {
		return installAnnotationProcessor(DmVersion.DM4);
	}
	
	private HeaderClause createRepoPluginHeader() {
		Attrs pluginAttrs = new Attrs();
		HeaderClause header = new HeaderClause("${ext.repositories.-plugin}", pluginAttrs);
		return header;
	}

	private HeaderClause createAnnotationPluginHeader(DmVersion dmVersion) {
		Attrs pluginAttrs = new Attrs();
		if(dmVersion.equals(DmVersion.DM3)) {
			pluginAttrs.put("path:", "${plugin-dir}/" + ANNOTATION_PROCESSOR_JAR_DM3);
		} else {
			pluginAttrs.put("path:", "${plugin-dir}/" + ANNOTATION_PROCESSOR_JAR_DM4);
		}
		pluginAttrs.put("build-import-export-service", "false");
		pluginAttrs.put("add-require-capability=true", "true");
		HeaderClause header = new HeaderClause(ANNOTATION_PLUGIN_NAME, pluginAttrs);
		return header;
	}

    @Override
    public InstallResult addDependencies(DmVersion version) {
        Dependency dependency = new Dependency("org.apache.felix.dependencymanager", getVersionString(version));
        return m_dependencyBuilder.addDependencies(Arrays.asList(new Dependency[] { dependency,
            new Dependency("osgi.core") }));
    }

	@Override
	public InstallResult addDependencies() {
		return addDependencies(DmVersion.DM4);
	}
	
	public void addComponent(boolean useAnnotations, FullyQualifiedName interfaceName, FullyQualifiedName componentName) {
		URL templateUri;
		if (useAnnotations) {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/annotatedcomponent.vm");
		} else {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/component.vm");
		}

		String activatorInterfaceName = null;
		
		createComponentFile(componentName.getClassName() + ".java", componentName, interfaceName, templateUri);

		if (!useAnnotations) {
			createActivator(componentName, new FullyQualifiedName(activatorInterfaceName));
		}
	}

	private void createComponentFile(String fileName, FullyQualifiedName fqn, FullyQualifiedName interfaceName, URL templateUri) {
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("interfaceName", interfaceName);
			context.put("packageName", fqn.getPackageName());

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fileName);
			Files.createDirectories(outputDir);

			Files.write(outputFile, template.getBytes());

		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName) {
		URL activatorTemplate = m_bundleContext.getBundle().getEntry("/templates/activator.vm");
		createComponentFile("Activator.java", fqn, interfaceName, activatorTemplate);
		String activator = "\nBundle-Activator: " + fqn.getPackageName() + ".Activator";
		
		try {
			Files.write(m_navigator.getBndFile(), activator.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

    @Override
    public InstallResult addRunDependencies(DmVersion version, Path runFilePath) {
        List<Dependency> dependencies = new ArrayList<>();
        dependencies.add(new Dependency("org.apache.felix.dependencymanager", getVersionString(version)));
        dependencies.add(new Dependency("org.apache.felix.dependencymanager.runtime", getVersionString(version)));
        dependencies.add(new Dependency("org.apache.felix.dependencymanager.shell", getVersionString(version)));
        dependencies.addAll(Dependency.fromStrings("org.apache.felix.metatype", "org.apache.felix.eventadmin",
            "org.apache.felix.configadmin", "org.apache.felix.log"));
        return m_dependencyBuilder.addRunDependency(dependencies, runFilePath);
    }

	@Override
	public InstallResult addRunDependencies(Path runFilePath) {
		return addRunDependencies(DmVersion.DM4, runFilePath);
	}

    /**
     * Get the version string to use for a {@link DmVersion}
     * 
     * @param version
     *        the {@link DmVersion} to get the version string for
     * @return The version string to use or null
     */
    private String getVersionString(DmVersion version) {
        if (DmVersion.DM3.equals(version)) {
            return "[3,4)";
        } else if (DmVersion.DM4.equals(version)) {
            return "[4,5)";
        }
        return null;
    }
}
