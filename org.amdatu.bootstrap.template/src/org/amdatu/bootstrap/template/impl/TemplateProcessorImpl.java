/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.template.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.amdatu.bootstrap.template.Template;
import org.amdatu.bootstrap.template.TemplateException;
import org.amdatu.bootstrap.template.TemplateProcessor;
import org.amdatu.bootstrap.template.TemplateResource;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class TemplateProcessorImpl implements TemplateProcessor {

	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@Override
	public void installTemplate(Template template, File file, Map<String, Object> properties) throws TemplateException {

		try {
			TemplateContext context = m_templateEngine.createContext();
			
			for (Entry<String, Object> prop : properties.entrySet()){
				context.put(prop.getKey(), prop.getValue());
			}
			
			for (TemplateResource resource: template.getResources()){
				String fileName = resource.getFileName();
				Path targetFile = file.toPath().resolve(fileName);
				
				if (targetFile.toFile().getName().startsWith("__.")){
					targetFile = targetFile.resolveSibling(targetFile.toFile().getName().substring(2));
				}
				
				if (fileName.endsWith(".vm")){
						targetFile = targetFile.resolveSibling(targetFile.toFile().getName().substring(0, targetFile.toFile().getName().length() -3));
						org.amdatu.template.processor.TemplateProcessor proc = m_templateEngine.createProcessor(resource.getUrl());
						targetFile.getParent().toFile().mkdirs();
						proc.generateFile(context, targetFile.toFile());
				}else{
					
					if (!targetFile.getParent().toFile().exists()){
						targetFile.getParent().toFile().mkdirs();
					}
					Files.copy(resource.getUrl().openStream(), targetFile, StandardCopyOption.REPLACE_EXISTING);
				}
				
				if(fileName.equals("gradlew")) {
					Set<PosixFilePermission> perms = new HashSet<>();
			        perms.add(PosixFilePermission.OWNER_READ);
			        perms.add(PosixFilePermission.OWNER_WRITE);
			        perms.add(PosixFilePermission.OWNER_EXECUTE);
			        perms.add(PosixFilePermission.GROUP_READ);
			        perms.add(PosixFilePermission.GROUP_EXECUTE);
			        perms.add(PosixFilePermission.OTHERS_READ);
					
			        try {
			        	Files.setPosixFilePermissions(targetFile, perms);
			        } catch(UnsupportedOperationException ex) {
			        	//Ignore, this only happens on file systems where permissions are not supported.
			        }
				}
			}
		}catch(Exception e ){
			throw new TemplateException(e);
		}
	}
	
}
