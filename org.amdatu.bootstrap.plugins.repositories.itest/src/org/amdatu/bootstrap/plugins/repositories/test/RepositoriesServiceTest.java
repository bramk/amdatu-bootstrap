package org.amdatu.bootstrap.plugins.repositories.test;

import static org.amdatu.testing.configurator.OSGiTestConfigurator.configureTest;
import static org.amdatu.testing.configurator.OSGiTestConfigurator.inject;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.repositories.DependencyType;
import org.amdatu.bootstrap.services.repositories.RepositoriesService;
import org.amdatu.bootstrap.testutils.bundles.BundleUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class RepositoriesServiceTest {
	private volatile BundleContext m_bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
	private volatile RepositoriesService m_repositoriesService;
	private volatile Navigator m_navigatorService;
	private File outputDir;
	private Path testWs;
	
	@Before
	public void setup() throws IOException {
		configureTest(this, inject(RepositoriesService.class), inject(Navigator.class));
		
		testWs = m_bundleContext.getDataFile("ws").toPath();
		BundleUtils.extract("resources/ws", m_bundleContext.getBundle(), testWs);
		
		m_navigatorService.changeDir(testWs);
		
		outputDir = m_bundleContext.getDataFile("result");
		FileUtils.deleteDirectory(outputDir);
		
		outputDir.mkdirs();
	}
	
	@Test
	public void testBndFileRunDeps() throws IOException {
		m_repositoriesService.materialize(testWs.resolve("project.a/bnd.bnd").toFile(), outputDir.toPath(), DependencyType.RUN);
		
		assertTrue(new File(outputDir, "index.xml.gz").exists());
		assertTrue(new File(outputDir, "org.apache.felix.configadmin-1.8.0.jar").exists());
		assertTrue(new File(outputDir, "org.apache.felix.dependencymanager-3.1.0.jar").exists());
	
	}		
	
	@Test
	public void testBndFileBuildDeps() throws IOException {
		m_repositoriesService.materialize(testWs.resolve("project.a/bnd.bnd").toFile(), outputDir.toPath(), DependencyType.BUILD);
		
		assertTrue(new File(outputDir, "index.xml.gz").exists());
		assertTrue(new File(outputDir, "org.apache.felix.dependencymanager-3.1.0.jar").exists());
		assertTrue(new File(outputDir, "org.apache.felix.gogo.runtime-0.10.0.jar").exists());
		
	}
	
	@Test
	public void testBndFileAllDeps() throws IOException {
		m_repositoriesService.materialize(testWs.resolve("project.a/bnd.bnd").toFile(), outputDir.toPath(), DependencyType.ALL);
		
		assertTrue(new File(outputDir, "index.xml.gz").exists());
		assertTrue(new File(outputDir, "org.apache.felix.configadmin-1.8.0.jar").exists());
		assertTrue(new File(outputDir, "org.apache.felix.dependencymanager-3.1.0.jar").exists());
		assertTrue(new File(outputDir, "org.apache.felix.gogo.runtime-0.10.0.jar").exists());
	}
	
	@Test
	public void testBndFileDepsNotSpecified() throws IOException {
		m_repositoriesService.materialize(testWs.resolve("project.a/bnd.bnd").toFile(), outputDir.toPath(), null);
		assertTrue(new File(outputDir, "index.xml.gz").exists());
	}
	
	@Test
	public void testBndRunFile() throws IOException {
		m_repositoriesService.materialize(testWs.resolve("run/test.bndrun").toFile(), outputDir.toPath(), DependencyType.RUN);
		
		assertTrue(new File(outputDir, "index.xml.gz").exists());
	
	}
}