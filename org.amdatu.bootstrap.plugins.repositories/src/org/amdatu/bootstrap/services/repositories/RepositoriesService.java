package org.amdatu.bootstrap.services.repositories;

import java.io.File;
import java.nio.file.Path;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public interface RepositoriesService {
	void materialize(File bndFile, Path target, DependencyType dependencyTypes);
	void materialize(File bndFile, Path target, DependencyType dependencyTypes, DependencyFilter filter);
	void materializeWorkspace(Path workspaceDir, File outputDir);
}
