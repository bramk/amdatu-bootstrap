/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.PluginDescription;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.ServiceReference;

@Component
public class DefaultPluginRegistry implements PluginRegistry {

	private final Map<ServiceReference<BootstrapPlugin>, BootstrapPlugin> m_plugins = new ConcurrentHashMap<>();

	@Override
	public List<PluginDescription> listPlugins() {
		return m_plugins.values().stream()
				.map(p -> {
					PluginDescriptionBuilder pluginDescriptionBuilder = new PluginDescriptionBuilder(p.getName());
					Arrays.stream(p.getClass().getMethods())
						.filter(m->m.isAnnotationPresent(Command.class) || m.getName().startsWith("_"))
						.forEach(pluginDescriptionBuilder::command);

					return pluginDescriptionBuilder.build();
				}).collect(Collectors.toList());
	}

	@ServiceDependency(removed = "removePlugin")
	public void addPlugin(ServiceReference<BootstrapPlugin> ref, BootstrapPlugin plugin) {
		m_plugins.put(ref, plugin);
	}

	public void removePlugin(ServiceReference<BootstrapPlugin> ref) {
		m_plugins.remove(ref);
	}

	@Override
	public Optional<BootstrapPlugin> getPlugin(String pluginName) {
		return m_plugins.values().stream().filter(p -> p.getName().equals(pluginName)).findAny();
	}

	@Override
	public Optional<PluginDescription> getPluginDescription(String pluginName) {
		return m_plugins.values().stream()
				.map(p -> {
					PluginDescriptionBuilder pluginDescriptionBuilder = new PluginDescriptionBuilder(p.getName());
					Arrays.stream(p.getClass().getMethods())
						.filter(m->m.isAnnotationPresent(Command.class) || m.getName().startsWith("_"))
						.forEach(pluginDescriptionBuilder::command);

					return pluginDescriptionBuilder.build();
				})
				.filter(p -> p.getName().equals(pluginName))
				.findAny();
	}
}
