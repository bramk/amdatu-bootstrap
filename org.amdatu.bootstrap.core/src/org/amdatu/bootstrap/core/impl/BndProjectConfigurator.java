/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.amdatu.bootstrap.services.ProjectConfigurator;
import org.amdatu.bootstrap.services.ResourceManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.ExportedPackage;
import aQute.bnd.properties.Document;

@Component
public class BndProjectConfigurator implements ProjectConfigurator {
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	
	@Override
	public void addPrivatePackage(Path bndFile, String privatePackage) {
		BndEditModel model = new BndEditModel();
		try {
			model.loadFrom(bndFile.toFile());
			model.addPrivatePackage(privatePackage);
			
			Document document = new Document(new String(Files.readAllBytes(bndFile)));
			model.saveChangesTo(document);
			
			m_resourceManager.writeFile(bndFile, document.get().getBytes());
			
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void addEntryToHeader(Path bndFile, String header, String entry) {
		BndEditModel model = new BndEditModel();
		try {
			model.loadFrom(bndFile.toFile());
			
			@SuppressWarnings("unchecked")
			List<String> genericGet = (List<String>)model.genericGet(header);
			
			if(genericGet == null) {
				genericGet = new ArrayList<>();
			}
		
			genericGet.add(entry);
			model.genericSet(header, genericGet);
			
			Document document = new Document(new String(Files.readAllBytes(bndFile)));
			model.saveChangesTo(document);
			
			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void addExportedPackage(Path bndFile, String exportPackage) {
		BndEditModel model = new BndEditModel();
		try {
			model.loadFrom(bndFile.toFile());
			model.addExportedPackage(new ExportedPackage(exportPackage, null));
			
			Document document = new Document(new String(Files.readAllBytes(bndFile)));
			model.saveChangesTo(document);
			
			m_resourceManager.writeFile(bndFile, document.get().getBytes());
			
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
