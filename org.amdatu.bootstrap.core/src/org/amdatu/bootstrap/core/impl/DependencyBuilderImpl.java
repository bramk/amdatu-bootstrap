/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import static org.amdatu.bootstrap.java8.Java8.uncheck;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.InstallResult.Builder;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.ResourceManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.VersionedClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;
import aQute.bnd.version.Version;
import aQute.bnd.version.VersionRange;

import com.google.common.collect.Sets;

@Component
public class DependencyBuilderImpl implements DependencyBuilder {
	private final Set<String> skipParseVersion = Sets.newHashSet("latest", "file", "null");
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@Override
	public InstallResult addDependency(String bsn) {
		return addDependency(bsn, null);
	}
	
	@Override
	public InstallResult addDependency(String bsn, String version) {
		return addDependency(new Dependency(bsn, version));
	}

	@Override
	public InstallResult addDependency(Dependency dependency, Path bndFile) {
		Builder installResult = InstallResult.builder();
		
		try {
			BndEditModel model = new BndEditModel();
			
			model.loadFrom(bndFile.toFile());
			
			List<VersionedClause> buildPath = model.getBuildPath();
			if(buildPath == null) {
				buildPath = new ArrayList<>();
			}
			
			boolean shouldBeAdded = true;
			for(VersionedClause versionedClause : buildPath) {
				if(versionedClause.getName().equals(dependency.getBsn())) {
					shouldBeAdded = false;
					break;
				}
			}
			
			if(shouldBeAdded) {
				Attrs attribs = new Attrs();
				
				if(dependency.getVersion() != null && !skipParseVersion.contains(dependency.getVersion())) {
					//Parse version and check validity
					new VersionRange(dependency.getVersion());
				}
				
				VersionedClause versionedClause = new VersionedClause(dependency.getBsn(), attribs);
				
				versionedClause.setVersionRange(dependency.getVersion());
				buildPath.add(versionedClause);

				model.setBuildPath(buildPath);
				
				Document document = new Document(new String(Files.readAllBytes(bndFile)));
				model.saveChangesTo(document);
				
				m_resourceManager.writeFile(bndFile, document.get().getBytes());
				
				installResult.addInstalled(dependency);
				
				if(m_navigator.getCurrentProject() != null) {
					sendEvent("org/amdatu/bootstrap/core/PROJECT_UPDATED", m_navigator.getCurrentProject().getName());
				}
			} else {
				installResult.addSkipped(dependency);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return installResult.build();
	}
	
	@Override
	public InstallResult addDependency(Dependency dependency) {
		return addDependency(dependency, m_navigator.getBndFile());
	}
	
	@Override
	public InstallResult updateBuildDependency(String bsn, String newVersion, Path bndFile) {

		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile.toFile());

			List<VersionedClause> buildPath = model.getBuildPath();
			if (buildPath == null) {
				// Nothing to update if build path is empty
				return InstallResult.empty();
			}

			buildPath = buildPath.stream()
					.map(v -> {
						if(v.getName().equals(bsn)) {
							v.setVersionRange(newVersion);
						}
						return v;
					})
					.collect(Collectors.toList());

			model.setBuildPath(buildPath);

			Document document = new Document(new String(Files.readAllBytes(bndFile)));
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());

			return InstallResult.builder().addUpdated(new Dependency(bsn, newVersion)).build();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public InstallResult updateRunDependency(String bsn, String newVersion, Path bndFile) {
		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile.toFile());

			List<VersionedClause> runBundles = model.getRunBundles();
			if (runBundles == null) {
				return InstallResult.empty();
			}

			// For run bundles it's best to fix at a specific version.
			runBundles = runBundles.stream()
					.map(v -> {
						if(v.getName().equals(bsn)) {
							v.setVersionRange("[" + newVersion + "," + newVersion + "]");
						}
						return v;
					})
					.collect(Collectors.toList());

			model.setRunBundles(runBundles);

			Document document = new Document(new String(Files.readAllBytes(bndFile)));
			model.saveChangesTo(document);
			m_resourceManager.writeFile(bndFile, document.get().getBytes());

			return InstallResult.builder().addUpdated(new Dependency(bsn, newVersion)).build();
		} catch (IOException | IllegalStateException e) {
			throw new RuntimeException(e);
		}
	}

    @Override
	public InstallResult addDependencies(Collection<Dependency> dependencies) {
		Builder builder = InstallResult.builder();
    	
    	for (Dependency dependency : dependencies) {
			builder.addResult(addDependency(dependency));
		}
    	
    	return builder.build();
	}
	
	@Override
    public InstallResult addRunDependency(String bsn, Path runFile) {
		return addRunDependency(bsn, null, runFile);
    }

    @Override
    public InstallResult addRunDependency(String bsn, String version, Path runFile) {
        return addRunDependency(new Dependency(bsn, version), runFile);
    }

    @Override
    public InstallResult addRunDependency(Dependency dependency, Path runFile) {
        return addRunDependency(Arrays.asList(dependency), runFile);
    }

    @Override
    public InstallResult addRunDependency(Collection<Dependency> dependencies, Path runFile) {
    	Builder builder = InstallResult.builder();
    	
		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(runFile.toFile());
			for (Dependency dependency : dependencies) {
				// find index of dependency
				int index = -1;
				List<VersionedClause> runBundles = model.getRunBundles();
				if(runBundles == null) {
					runBundles = new ArrayList<>();
				}

				for (VersionedClause versionedClause : runBundles) {
					if (versionedClause.getName().equals(dependency.getBsn())) {
						index = runBundles.indexOf(versionedClause);
						break;
					}
				}

				VersionedClause versionedClause = new VersionedClause(dependency.getBsn(), new Attrs());
				if (dependency.getVersion() != null && !skipParseVersion.contains(dependency.getVersion())) {
					// fix the version to exactly that version
					if(!dependency.getVersion().contains("[")) {
						versionedClause.setVersionRange("[" + dependency.getVersion() + "," + dependency.getVersion() + "]");
					} else {
						versionedClause.setVersionRange(dependency.getVersion());
					}
				}
				if (index > -1) {
					runBundles.set(index, versionedClause);
					builder.addUpdated(dependency);
					
					//Project might be null for non-bnd projects
					if(m_navigator.getCurrentProject() != null) {
						sendEvent("org/amdatu/bootstrap/core/PROJECT_UPDATED", m_navigator.getCurrentProject().getName());
					}
				} else {
					runBundles.add(versionedClause);
					builder.addInstalled(dependency);
				}
				model.setRunBundles(runBundles);
			}
			Document document = new Document(new String(Files.readAllBytes(runFile)));
			model.saveChangesTo(document);
			m_resourceManager.writeFile(runFile, document.get().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
        
        return builder.build();
    }
    

	@Override
	public boolean hasDependency(Dependency dependency, Path bndFile) {
		try {
			BndEditModel model = new BndEditModel();
			
			model.loadFrom(bndFile.toFile());
			
			List<VersionedClause> buildpath = model.getBuildPath();
			if(buildpath == null) {
				return false;
			}
			
			for (VersionedClause versionedClause : buildpath) {
				if(versionedClause.getName().matches(dependency.getBsn())) {
					return true;
				}
			}
			
			return false;
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

    @Override
	public boolean hasDependency(Dependency dependency) {
    	Path bndFile = m_navigator.getBndFile();
    	
    	return hasDependency(dependency, bndFile);
	}

	@Override
	public boolean hasDependency(String bsn, String version) {
		return hasDependency(new Dependency(bsn, version));
	}

	@Override
	public boolean hasDependency(String bsn) {
		return hasDependency(new Dependency(bsn));
	}

	@Override
	public boolean hasDependencies(Collection<Dependency> dependencies) {
		for (Dependency dependency : dependencies) {
			if(!hasDependency(dependency)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public Set<Dependency> listProjectBuildDependencies() {
		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(m_navigator.getBndFile().toFile());
			
			return model.getBuildPath().stream()
				.map(c -> new Dependency(c.getName(), c.getVersionRange()))
				.collect(Collectors.toSet());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Set<Dependency> listWorkspaceBuildDependencies() {
		try {
			 return m_navigator.getProjectsInWorkspace().stream()
					.flatMap(p -> uncheck(() -> {
						BndEditModel model = new BndEditModel();
						File bnd = p.getFile("bnd.bnd");
						model.loadFrom(bnd);
						
						return Optional.ofNullable(model.getBuildPath()).orElse(Collections.emptyList()).stream();
					}))
					.map(c -> new Dependency(c.getName(), c.getVersionRange()))
					.collect(Collectors.toSet());
			 
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Set<Dependency> listRunDependencies(Path runFile) {
		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(runFile.toFile());
			
			return model.getRunBundles().stream()
					.map(v -> new Dependency(v.getName(), v.getVersionRange()))
					.collect(Collectors.toSet());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	

    private void sendEvent(String topicName, String name) {
        Map<String, Object> props = new HashMap<>();
        props.put("projectname", name);
        Event event = new Event(topicName, props);
        m_eventAdmin.sendEvent(event);
    }

	@Override
	public SortedSet<Version> listAvailableVersions(String bsn) {
		SortedSet<Version> result = new TreeSet<>();
		
		//Clumsy way of putting versions in a SortedSet to work around a default method conflict with List and SortedSet
		List<SortedSet<Version>> collect = m_navigator.getCurrentWorkspace().getRepositories().stream()
				.map(r -> uncheck(() -> r.versions(bsn)))
				.collect(Collectors.toList());
		collect.forEach(result::addAll);	
			
		
		return result;
	}

	@Override
	public Optional<Version> listHighestVersion(String bsn) {
		SortedSet<Version> versions = listAvailableVersions(bsn);
		if(!versions.isEmpty()) {
			return Optional.of(versions.last());
		} else {
			return Optional.empty();
		}
	}

}	
