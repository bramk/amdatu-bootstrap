/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.installer.git;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.gradle.GradleService;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.eclipse.jgit.api.Git;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.Project;

@Component
public class GitInstallerPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@ServiceDependency
	private volatile GradleService m_gradleService;
	
	interface InstallParams extends Parameters{
		@Required
		String gitUrl();
		
	}
	
	@Command
	public void installFromUrl(InstallParams params, Prompt prompt) throws Exception {
		Path buildDir = Files.createTempDirectory(null);
		
		Path currentDir = m_navigator.getCurrentDir();

		prompt.printf("Cloning git repository: %s", params.gitUrl());
		
		Future<?> gitClone = Executors.newFixedThreadPool(1).submit(() -> {
			try {
				Git.cloneRepository().setURI(params.gitUrl()).setDirectory(buildDir.toFile()).call();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
		while(!gitClone.isDone()) {
			prompt.printStatus(0,0);
			Thread.sleep(500);
		}
		
		prompt.println("Building plugin workspace...");
		m_navigator.changeDir(buildDir);
		
		m_gradleService.build(prompt, "build");
		List<File> plugins = new ArrayList<>();

		for (Project project : m_navigator.getCurrentWorkspace().getBuildOrder()) {
			File[] buildFiles = project.getBuildFiles();
			for (File file : buildFiles) {
				plugins.add(file);
			}
		}

		File pluginToInstall = prompt.askChoice("Which jar do you want to install?", 0, plugins);
		Files.copy(pluginToInstall.toPath(), m_bundleContext.getDataFile(pluginToInstall.getName()).toPath(),
				StandardCopyOption.REPLACE_EXISTING);
		Bundle installBundle = m_bundleContext.installBundle(m_bundleContext.getDataFile(pluginToInstall.getName()).toURI().toString());
		installBundle.start();
		
		prompt.println("Plugin " + installBundle.getSymbolicName() + " successfully installed");
		
		m_navigator.changeDir(currentDir);
	}
	
	@Override
	public String getName() {
		return "gitplugins";
	}

}
