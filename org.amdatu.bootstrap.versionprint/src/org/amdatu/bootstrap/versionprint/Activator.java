package org.amdatu.bootstrap.versionprint;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator{

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Amdatu Bootstrap R8");
		
		if(context.getProperty("info") == null) {
			
			int width = 300;
			int height = 30;
	
			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics g = image.getGraphics();
			g.setFont(new Font("SansSerif", Font.BOLD, 24));
	
			Graphics2D graphics = (Graphics2D) g;
			graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
						RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			graphics.drawString("AMDATU BOOTSTRAP", 10, 20);
	
			for (int y = 0; y < height; y++) {
				StringBuilder sb = new StringBuilder();
				for (int x = 0; x < width; x++) {
	
					sb.append(image.getRGB(x, y) == -16777216 ? " " : "$");
						
				}
	
				if (sb.toString().trim().isEmpty()) {
					continue;
				}
	
				System.out.println(sb);
			}
		} else {
			System.exit(0);
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		
	}

}
