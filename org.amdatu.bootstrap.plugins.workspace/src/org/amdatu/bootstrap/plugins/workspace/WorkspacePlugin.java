/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.workspace;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.DynamicSelect;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.template.Template;
import org.amdatu.bootstrap.template.TemplateException;
import org.amdatu.bootstrap.template.TemplateProcessor;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.commons.io.IOUtils;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

@Component
public class WorkspacePlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile TemplateProcessor m_templateProcessor;

	@Inject
	private volatile BundleContext m_bundleContext;

	private final Set<TemplateProvider> m_workspaceTemplateProviders = new HashSet<>();

	interface InitParameters extends Parameters {

		@DynamicSelect("listTemplates")
		String template();
	}

	public List<EnumValue> listTemplates() {
		return m_workspaceTemplateProviders.stream()
				.flatMap(t -> t.listTemplates().stream())
				.map(t-> new EnumValue(t.getName(), t.getName()))
				.collect(Collectors.toList());
	}

	@Command
	@Description("Initialize a new workspace (with cnf dir) in the current directory")
	public File init(InitParameters parameters, Prompt prompt) {
		if (m_navigator.getWorkspaceDir() != null) {
			prompt.println("Can't init workspace (already in a workspace)");
			return null;
		}

		File file = m_navigator.getCurrentDir().toFile();

		try {
			Template template = findTemplateByName(parameters.template());
			
			m_templateProcessor.installTemplate(template, file, new HashMap<String, Object>());
		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}

		m_navigator.changeDir(file.toPath());

		for (File project : file.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return new File(pathname, ".project").exists();
			}
		})) {
			sendEvent("org/amdatu/bootstrap/core/PROJECT_CREATED", project.getName());
		}

		return file;
	}

	private Template findTemplateByName(String templateName) {
		Optional<Template> template = m_workspaceTemplateProviders.stream()
				.flatMap(t -> t.listTemplates().stream())
				.filter(t -> t.getName().equals(templateName))
				.findAny();
		
		if(!template.isPresent()) {
			throw new RuntimeException("Template '" + templateName + "' not found");
		}
		
		return template.get();
	}

	interface CreateParameters extends InitParameters {
		@Description("Name of the workspace")
		String name();
	}

	@Command
	@Description("Initialize a new workspace (with cnf dir) in a new directory")
	public File create(CreateParameters parameters, Prompt prompt) {
		if (m_navigator.getWorkspaceDir() != null) {
			prompt.println("Can't create workspace (already in a workspace)");
			return null;
		}

		String name = parameters.name();
		
		File file = new File(m_navigator.getCurrentDir().toFile(), name);
		if (file.exists()) {
			prompt.println("Can't create workspace (file exists)");
			return null;
		}

		if (!file.mkdir()) {
			prompt.println("Can't create workspace (couldn't create workspace dir)");
			return null;
		}

		try {
			Template template = findTemplateByName(parameters.template());
			HashMap<String, Object> templateProperties = new HashMap<String, Object>();
			
			if(parameters.template().equals("Amdatu Bootstrap Plugin")) {
				System.out.println("Bootstrap plugin");
				
				String wsLocation = prompt.askString("A plugin workspace uses a Bootstrap workspace as repository. Specify the directory of your Amdatu Bootstrap workspace");
				templateProperties.put("bootstrapworkspace", wsLocation);
			}
			
			
			m_templateProcessor.installTemplate(template, file, templateProperties);
		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}

		m_navigator.changeDir(file.toPath());
		return file;
	}

	@Command(scope = Scope.WORKSPACE)
	@Description("Add Amdatu repositories to the workspace configuration")
	public void addRepositories(Prompt prompt) {
		Path mainBnd = m_navigator.getWorkspaceDir().resolve("cnf/ext/repositories.bnd");

		String repoContents = null;
		try (FileInputStream in = new FileInputStream(mainBnd.toFile())) {
			repoContents = new String(Files.readAllBytes(mainBnd), "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		if (repoContents.contains("-plugin.amdatu:")) {
			prompt.println("Repository plugin already registered, skipping");
		} else {
			URL resource = m_bundleContext.getBundle().getResource("repo-templates/amdatu.txt");
			try (InputStream in = resource.openStream()) {
				byte[] bytes = IOUtils.toByteArray(in);
				Files.write(mainBnd, bytes, StandardOpenOption.APPEND);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void sendEvent(String topicName, String name) {
		Map<String, Object> props = new HashMap<>();
		props.put("projectname", name);
		Event event = new Event(topicName, props);
		m_eventAdmin.sendEvent(event);
	}

	@ServiceDependency(removed = "workspaceTemplateProviderRemoved", filter = "(type=workspace)")
	private void workspaceTemplateProviderAdded(TemplateProvider templateProvider) {
		m_workspaceTemplateProviders.add(templateProvider);
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void workspaceTemplateProviderRemoved(TemplateProvider templateProvider) {
		m_workspaceTemplateProviders.remove(templateProvider);
	}

	@Override
	public String getName() {
		return "workspace";
	}

}
