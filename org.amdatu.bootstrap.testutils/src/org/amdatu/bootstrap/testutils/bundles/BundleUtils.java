package org.amdatu.bootstrap.testutils.bundles;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;

import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;

public class BundleUtils {
	public static void extract(String path, Bundle bundle, Path outputDir) {
		Enumeration<String> entryPaths = bundle.getEntryPaths(path);
		extract(path, entryPaths, bundle, outputDir.toFile());
	}
	
	private static void extract(String rootPath, Enumeration<String> entryPaths, Bundle bundle, File outputDir) {
		while(entryPaths.hasMoreElements()) {
			String entry = entryPaths.nextElement();
			String newEntryPath = entry.replace(rootPath, "");
			
			if(entry.endsWith("/")) {
				Enumeration<String> subPath = bundle.getEntryPaths(entry);
				File dir = new File(outputDir, newEntryPath);
				try {
					Files.createDirectories(dir.toPath());
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				
				if(subPath != null) {
					extract(rootPath, subPath, bundle, outputDir);
				}
			} else {
				try(InputStream in = bundle.getResource(entry).openStream();
					FileOutputStream out = new FileOutputStream(new File(outputDir, newEntryPath))) {
					IOUtils.copy(in, out);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}
