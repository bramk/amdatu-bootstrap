/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.services;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.amdatu.bootstrap.command.InstallResult;

import aQute.bnd.annotation.ProviderType;
import aQute.bnd.version.Version;

/**
 * Service that takes care of managing build and run dependencies in a project's bnd file. 
 */
@ProviderType
public interface DependencyBuilder {

	InstallResult addDependency(String bsn);

	InstallResult addDependency(String bsn, String version);

	/**
	 * This will add or update a Dependency to the bnd build path of the current project.
	 * @param dependency The dependency to add
	 * @return An InstallResult listing the installed/updated/ignored dependencies.
	 */
	InstallResult addDependency(Dependency dependency);
	
	InstallResult addDependency(Dependency dependency, Path bndFile);

	InstallResult addDependencies(Collection<Dependency> dependencies);
	
	InstallResult addRunDependency(String bsn, Path runFile);

	InstallResult addRunDependency(String bsn, String version, Path runFile);

	InstallResult addRunDependency(Dependency dependency, Path runFile);

    /**
     * This will add or update a Dependency to the bnd run configuration of the specified bnd file.
     * @param dependencies List of dependencies to add
     * @param runFile The .bnd or .bndrun file to add to.
     * @return An InstallResult listing the installed/updated/ignored dependencies.
     */
	InstallResult addRunDependency(Collection<Dependency> dependencies, Path runFile);

	boolean hasDependency(Dependency dependency, Path project);
	
	boolean hasDependency(Dependency dependency);

	boolean hasDependency(String bsn, String version);

	boolean hasDependency(String bsn);
	
	boolean hasDependencies(Collection<Dependency> dependencies);
	
	Set<Dependency> listProjectBuildDependencies();
	
	Set<Dependency> listWorkspaceBuildDependencies();
	
	Set<Dependency> listRunDependencies(Path runFile);
	
	Set<Version> listAvailableVersions(String bsn);
	
	Optional<Version> listHighestVersion(String bsn);

	InstallResult updateBuildDependency(String bsn, String newVersion, Path bndFile);

	InstallResult updateRunDependency(String bsn, String newVersion, Path bndFile);
}
