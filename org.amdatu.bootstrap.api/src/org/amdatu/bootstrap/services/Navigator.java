/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.services;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.EnumValue;

import aQute.bnd.annotation.ProviderType;
import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;

/**
 * Service to navigate the file system and get information about the current workspace and project.
 */
@ProviderType
public interface Navigator {
	public final static String CHANGEDIR_TOPIC = "org/amdatu/bootstrap/navigation";
	
	Workspace getCurrentWorkspace();

	Project getCurrentProject();

	Path getCurrentDir();

	void changeDir(Path newDir);

	Path getProjectDir();

	Path getWorkspaceDir();

	List<Path> getBndRunFiles();

	List<File> listProjectBndFiles();

	List<Path> findWorkspaceRunConfigs();

	Path getHomeDir();

	Path getPreviousDir();
	
	Path getBndFile();
	
	Scope getCurrentScope();

	List<EnumValue> findWorkspaceBundles();
	
	List<Project> getProjectsInWorkspace();
	
	List<EnumValue> findProjectBndFiles();
	
	List<String> getRecentWorkspaces();
}
