// <reference path="typeScriptDefenitions/libs.d.ts" />

import PluginsService = require('PluginsService')
import _ = require('lodash')
import OutputMessage = require("plugins/OutputMessage")
import DependenciesOutputMessage = require("plugins/DependenciesOutputMessage")

class PluginsController {
    dirs : string[] = [];
    currentDir : string;

    plugins = [];
    command : string;
    arguments = [];
    argValues = {};
    output : OutputMessage[] = [];
    currentScope = "GLOBAL";
    files : string[];
    recentWorkspaces : string[];
    backendAvailable = true;
    drives : string[];
    selectedDrive : string;

    static $inject = ['PluginsService', '$scope', '$modal', '$timeout'];

    constructor(private pluginsService : PluginsService, private scope : ng.IScope, private modal : ng.ui.bootstrap.IModalService, private $timeout : ng.ITimeoutService) {
        pluginsService.getPlugins().flatMap(plugin => {
            return Rx.Observable.fromArray(plugin.commandDescription).flatMap(c => {
                c.name = plugin.name + "-" + c.name;
                return Rx.Observable.of(c);
            });
        }).subscribe(p => this.plugins.push(p));

        this.scope.$watch('currentDir', () => {
            pluginsService.listFiles().subscribe(files => {
                this.files = files;

            });
        });


        pluginsService.connect().do(r => {

          switch (r.type) {
            case "command-result":
              this.handleCommandResult(r.body);
              break;
            case "prompt":
              this.handlePrompt(r.body);
              break;
            case "prompt-multi":
              this.handlePrompt(r.body);
              break;
            case "scope-update":
              this.handleScopeUpdate(r.body);
              break;
            case "error":
              this.handleError(r.body);
              break;
            case "feedback":
              this.handleFeedback(r.body);
              break;
            case "status":
              this.handleStatus(r.body);
              break;
          }

        }).subscribe();

        this.listDirs();
        this.listDrives();
        this.pwd();
        this.getCurrentScope();
        this.getRecentWorkspaces();
        this.ping();
    }

    private ping() {
      this.$timeout(() => {
        this.pluginsService.ping().timeout(5000).subscribe(() => {
          //Connection is back, reload to reconnect
          if(!this.backendAvailable) {
            window.location.reload();
          }

          this.backendAvailable = true;
        }, () => {
          this.backendAvailable = false;
        });
        this.ping();
      }, 2000);
    }

    switchWorkspace(workspace) {
        this.navigate(workspace);
    }

    switchDrive(drive) {
        this.navigate(drive);
    }

    private handleCommandResult(commandResult) {
      if(commandResult.commandName == 'navigation-cd') {
        this.currentDir = commandResult.result;
        this.dirs = [];
        this.listDirs();
      } else {
        this.writeToOutput(commandResult.result);
        this.scope.$digest();
      }
    }

    private handlePrompt(prompt) {
      var modalInstance = this.modal.open({
        templateUrl: 'myModalContent.html',
        controller: 'FeedbackModalController',
        windowClass: 'modal',
        keyboard: true,
        resolve: {
          question: () => prompt
        }
      });

      modalInstance.result.then(selectedItem => {

        var msg;
        if(prompt.type != "java.util.Map") {
          msg = {
            type: 'promptquestion',
            answer: selectedItem
          }
        } else {
          msg = {
            type: 'promptmultiquestion',
            answers: selectedItem
          }
        }

        this.pluginsService.sendAnswer(msg);
      }, () => {
          this.pluginsService.sendCancel();
      });
    }

    private handleScopeUpdate(update) {
      this.currentScope = update['newScope'];
      this.getRecentWorkspaces();
    }

    private handleError(error) {
      this.writeErrorToOutput(error.message);
      this.scope.$digest();
    }

    private handleFeedback(feedback) {
      this.output.push(new OutputMessage(feedback));
      this.scope.$digest();
    }

    private handleStatus(status) {
      if (this.output[this.output.length - 1].message.indexOf("#") != -1) {
        this.output[this.output.length - 1].message += "#";
      } else {
        this.output.push(new OutputMessage("#"));
      }
      this.scope.$digest();
    }

    private listDirs() {
        this.pluginsService.ls()
            .map(d => d.substr(d.lastIndexOf("/") + 1))
            .subscribe(d => {
                if(this.dirs.indexOf(d) == -1) {
                    this.dirs.push(d)
                }
            });
    }

    private listDrives() {
        this.pluginsService.listDrives().subscribe(d => {
            this.drives = d;
            this.selectedDrive = d[0];
        });
    }

    private pwd() {
      this.pluginsService.pwd()
          .subscribe(d => {
            this.currentDir = d;
          });
    }

    private getCurrentScope() {
        this.pluginsService.getCurrentScope().subscribe(s => {
            this.currentScope = s;
        });
    }

    private getRecentWorkspaces() {
        this.pluginsService.getRecentWorkspaces().subscribe(workspaces => {
            this.recentWorkspaces = workspaces;
        });
    }

    execute() {
        this.pluginsService.execute(this.command, this.argValues);

        localStorage['argvalues-' + this.command] = JSON.stringify(this.argValues);

        this.command = null;
        this.arguments = [];
        this.argValues = {};
    }

    navigate(dir) {
        this.dirs = [];
        this.pluginsService.navigate(dir);
    }

    private writeToOutput(result) {
       if(result && result.installedDependencies) {
           var installed = new DependenciesOutputMessage("Installed", result.installedDependencies);
           var updated = new DependenciesOutputMessage("Updated", result.updatedDependencies);
           var skipped = new DependenciesOutputMessage("Skipped", result.skippedDependencies);

           this.output.push(installed);
           this.output.push(skipped);
           this.output.push(updated);


       } else if(_.isArray(result)) {
          _.each(result, (r:string) => {
            this.output.push(new OutputMessage(r));
          });
       } else {
           this.output.push(new OutputMessage(result));
       }
    }

    private writeErrorToOutput(result) {
        this.output.push(new OutputMessage(result, 3));
    }

    showArgs() {
        this.arguments = [];

        Rx.Observable.fromArray(this.plugins)
            .filter(p => p.name == this.command)
            .flatMap(p => {
                return Rx.Observable.fromArray(p.arguments)
            })
            .filter(a => a['name'].lastIndexOf("_", 0) != 0 && a['name'] != "arg1")
            .subscribe((a : any) => {
                if(a['callback']) {
                  var plugin = this.command.substr(0, this.command.indexOf("-"));
                  this.pluginsService.getOptions(plugin, a['callback']).subscribe(options => {
                      a['options'] = options;
                      this.arguments.push(a);
                      if(a.options && a.options.length > 0) {
                          this.argValues[a.name] = a.options[0].name;
                      }
                      });

                } else {
                  this.arguments.push(a);
                    console.log(a);
                    if(a.options && a.options.length > 0) {
                        this.argValues[a.name] = a.options[0].name;
                    }
                }

                var storedValue = localStorage['argvalues-' + this.command]
                if(storedValue) {

                  var values = JSON.parse(storedValue);
                  _.each(this.arguments, (arg) => {
                      //Check for stored values
                      var argValue = values[arg.name];
                      if(argValue && arg.store) {
                        this.argValues[arg.name] = argValue;
                      }

                  });
                }

            });
    }


    clearOutput() {
      this.output = [];
    }

    help() {
            var modalInstance = this.modal.open({
                templateUrl: 'help.html',
                controller: 'HelpModalController',
                controllerAs: 'helpController',
                windowClass: 'modal',
                keyboard: true,
                size: 'lg'

            });

            modalInstance.result.then(() => {
            });


    }

}

export = PluginsController
