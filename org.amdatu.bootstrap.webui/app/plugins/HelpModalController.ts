// <reference path="typeScriptDefenitions/libs.d.ts" />
import PluginsService = require('PluginsService')

class HelpModalController {
    static $inject = ['$scope','$modalInstance', 'PluginsService'];

    plugins : Plugin[];

    constructor($scope, private $modalInstance : ng.ui.bootstrap.IModalServiceInstance, pluginsService : PluginsService) {
        pluginsService.getPlugins().toArray().subscribe(plugins => {
            this.plugins = plugins;
            console.log(plugins);
        });
    }

    ok() {
        console.log("closing");
        this.$modalInstance.close('');

    }

}

export = HelpModalController
